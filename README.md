# GCC3-Website

This is [the website](https://tedsakaryagcc3.xyz) I developed for the TED Global Citizenship Club, Challenge 3. You are free to contribute in any way you'd like.

July 2021 Update: Due to the GCC Interchange and the proposal evaluations being over, the website is no longer up, and the project is unmaintaned.
